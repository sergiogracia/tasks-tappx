<?php

namespace App\Tests\Model;

use App\Model\TaskDTO;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiControllerTest extends WebTestCase
{
    public function testGetAll()
    {
        $client = $this->createClient();
        $client->request('GET', '/api/tasks/get-all');
        $response = $client->getResponse();
        $content = json_decode($response->getContent());
        $this->assertResponseStatusCodeSame(200, $response);
        $this->assertEquals(200, $content->code);
        $this->assertEquals('Tasks successfully recovered', $content->message);
        $this->assertIsArray($content->data);
    }

    public function testGetOne()
    {
        $client = $this->createClient();
        $client->request('GET', '/api/tasks/get-one/62d94f3a3ef18');
        $response = $client->getResponse();
        $content = json_decode($response->getContent());
        $this->assertResponseStatusCodeSame(200, $response);
        $this->assertEquals(200, $content->code);
        $this->assertEquals('Task successfully recovered', $content->message);
        $this->assertIsObject($content->data);
        $this->assertEquals('62d94f3a3ef18', $content->data->id);
    }

    public function testRegister()
    {
        $client = $this->createClient();
        $client->request('POST', '/api/tasks/register', [
            'title' => 'test task',
            'done' => true
        ]);
        $response = $client->getResponse();
        $content = json_decode($response->getContent());
        $this->assertResponseStatusCodeSame(200, $response);
        $this->assertEquals(200, $content->code);
        $this->assertEquals('Task successfully registered', $content->message);
        $this->assertIsObject($content->data);
        $this->assertEquals('test task', $content->data->title);
        $this->assertEquals(true, $content->data->done);
    }
}