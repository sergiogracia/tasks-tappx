<?php

namespace App\Tests\Model;

use App\Model\TaskDTO;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TaskDTOTest extends KernelTestCase
{
    public function testEmptyTask()
    {
        $task = new TaskDTO();

        // nos aseguramos que la tarea está vacía
        $this->assertNull($task->getId());
        $this->assertNull($task->getTitle());
        $this->assertNull($task->getCreatedAt());
        $this->assertNull($task->getUpdatedAt());
        $this->assertNull($task->getDone());
    }

    public function testFilledTask()
    {
        $task = new TaskDTO();
        $date = new DateTime();
        $task->setId('t1');
        $task->setTitle('task1');
        $task->setCreatedAt($date);
        $task->setUpdatedAt($date);
        $task->setDone(false);

        // nos aseguramos que la tarea está vacía
        $this->assertEquals('t1', $task->getId());
        $this->assertEquals('task1', $task->getTitle());
        $this->assertEquals($date, $task->getCreatedAt());
        $this->assertEquals($date, $task->getUpdatedAt());
        $this->assertFalse($task->getDone());
    }

    public function testDataTypeTask()
    {
        $task = new TaskDTO();
        $date = new DateTime();
        $task->setId('t1');
        $task->setTitle('task1');
        $task->setCreatedAt($date);
        $task->setUpdatedAt($date);
        $task->setDone(false);

        // nos aseguramos que la tarea está vacía
        $this->assertIsString($task->getId());
        $this->assertIsString($task->getTitle());
        $this->assertIsObject($task->getCreatedAt());
        $this->assertIsObject($task->getUpdatedAt());
        $this->assertIsBool($task->getDone());
    }
}