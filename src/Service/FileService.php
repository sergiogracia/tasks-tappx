<?php

namespace App\Service;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;

class FileService
{
    /** @var Filesystem $filesystem */
    private $filesystem;

    /** @var string $path */
    private $path;

    public function __construct(Filesystem $filesystem, string $path)
    {
        $this->filesystem = $filesystem;
        $this->path = $path;
    }

    public function readFile(string $fileName): ?string
    {
        $fullPath = sprintf('%s%s', $this->path, $fileName);
        if ($this->filesystem->exists($fullPath)) {
            return (new File($fullPath))->getContent();
        }
        return null;
    }

    public function writeFile(string $fileName, mixed $data): bool
    {
        $fullPath = sprintf('%s%s', $this->path, $fileName);
        if ($this->filesystem->exists($fullPath)) {
            $this->filesystem->dumpFile($fullPath, $data);
            return true;
        }
        return false;
    }
}