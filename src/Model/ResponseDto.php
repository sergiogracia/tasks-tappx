<?php
namespace App\Api\Form\Model;

use Symfony\Component\Serializer\Annotation\Groups;

class ResponseDto {
    
    /**
     * 
     * @var int
     */
    public $code;

    /**
     * 
     * @var string
     */
    public $message;

    /**
     * 
     */
    public $data;


    public function __construct(){}

    public function setCode(int $code){
        $this->code = $code;
    }

    public function setMessage(string $message){
        $this->message = $message;
    }

    public function setData($data){
        $this->data = $data;
    }

    public function getData(){
        return $this->data;
    }

}